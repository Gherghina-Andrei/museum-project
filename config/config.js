import firebase from 'firebase'

//api details
const firebaseConfig = {
    apiKey: "AIzaSyAemMSVmAbpZKRE1e8nTMnVG_e4_GFDFAs",
    authDomain: "myfirstproject-ee28e.firebaseapp.com",
    databaseURL: "https://myfirstproject-ee28e.firebaseio.com",
    projectId: "myfirstproject-ee28e",
    storageBucket: "myfirstproject-ee28e.appspot.com",
    messagingSenderId: "76450794726",
    appId: "1:76450794726:web:7f293e30ac8294cbea3f27",
    measurementId: "G-RLKJNRVMQ8"
  };

firebase.initializeApp(firebaseConfig)

export const f = firebase;
export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
import React, { Component } from 'react';
import {
  Alert,
  Text,
  TextInput,
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import * as Facebook from 'expo-facebook';

import { LinearGradient } from 'expo-linear-gradient';
import { FontAwesome } from '@expo/vector-icons';
import {f, auth, database} from '../../config/firebase';

const COLORS = {
  WHITE: '#FFF',
  BLACK: '#000',
  BLUE: '#69B1D6',
  ORANGE: '#FE8E4E',
  RED: '#FD696E',
  GREY: '#AFAFAF',
  DARK_GREY: '#90919E',
  FACEBOOK: '#3A5896',
};

const SIZES = {
  BASE: 6,
  FONT: 12,
  TITLE: 24,
  SUBTITLE: 11,
  LABEL: 12,
  PADDING: 12,
};

const FACEBOOK_APP_ID = '221998685637394';
const API_URL = 'http://5e08ac18434a370014168b98.mockapi.io/api/v1';

export default class SignupScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };



  constructor(props)
  {
    super(props);
    this.state = {
      loggedin: false,
      loading: false,
      email: 'TESTEMAILADDRESS@YAHOO.COM',
      password: '123456',
      confirmPass: '12345',
    };

    //this.registerUser('TESTEMAILADDRESS@YAHOO.COM','123456');
    var that = this;

    f.auth().onAuthStateChanged(function(user){

      if(user){
        console.log('logged in');
        that.setState({
          loggedin : true
        });
      }else{
        console.log(' NOT logged in');
        that.setState({
          loggedin : false
        });
      }


    });

  }

  openSignin = () => {
    this.props.navigation.navigate('Signin');
  };

  createUserObj = (userObj, email) => {
    console.log(userObj, email, userObj.uid);
    let uObj = {
      firstName: 'First Name',
      lastName: 'Last name',
      avatar: 'user avatar',
      email: email,
      role: 'user'

    };
    database.ref('users').child(userObj.uid).set(uObj);
  }

    registerUser = (email, password,confirmPass) => {

    console.log(email.password);

    if(password == confirmPass)
    {
      auth.createUserWithEmailAndPassword(email,password)
          .then((userObj) => this.createUserObj(userObj.user, email))
          .catch((error) => alert(error));


    }else
    {
      alert(`Passwords do not match`);
    }



  }

  signUserOut = () => {
    auth.signOut()
        .then(()=>{
          console.log('logged out!')
        }).catch((error) =>{
      console.log('error',error)
    });
  }

  handleFacebook = async () => {
    try {
      await Facebook.initializeAsync(FACEBOOK_APP_ID);
      const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  };






  renderInputs() {
    const { email, password, loading, confirmPass } = this.state;
    const isValid = email && password;

    return (
        <>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Email address</Text>
            <TextInput
                value={email}
                style={styles.input}
                placeholder="you@email.com"
                placeholderTextColor={COLORS.DARK_GREY}
                onChangeText={value => this.setState({ email: value })}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Password</Text>
            <TextInput
                secureTextEntry
                value={password}
                style={styles.input}
                placeholderTextColor={COLORS.DARK_GREY}
                onChangeText={value => this.setState({ password: value })}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Confirm Password</Text>
            <TextInput
                secureTextEntry
                value={confirmPass}
                style={styles.input}
                placeholderTextColor={COLORS.DARK_GREY}
                onChangeText={value => this.setState({ confirmPass: value })}
            />
          </View>
          <TouchableOpacity
              disabled={!isValid}
              style={{ marginTop: SIZES.PADDING * 1.5 }}
              onPress={() => this.registerUser(this.state.email,this.state.password,this.state.confirmPass)}>
            <LinearGradient
                style={[styles.button, styles.signin]}
                colors={isValid ? [COLORS.ORANGE, COLORS.RED] : [COLORS.GREY, COLORS.DARK_GREY]}
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}>
              {loading ? (
                  <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
              ) : (
                  <Text
                      style={{
                        fontWeight: '500',
                        letterSpacing: 0.5,
                        color: COLORS.WHITE,
                        backgroundColor: 'transparent',
                      }}>
                    SignUp!
                  </Text>
              )}
            </LinearGradient>
          </TouchableOpacity>
        </>
    );
  }

  renderSocials() {
    return (
        <View style={styles.social}>
          <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.button, styles.facebook]}
              onPress={() => this.handleFacebook()}>
            <View style={{ flexDirection: 'row' }}>
              <FontAwesome size={18} name="facebook-square" color={COLORS.WHITE} />
              <Text style={styles.socialLabel}>Facebook</Text>
            </View>
          </TouchableOpacity>
        </View>
    );
  }

  render() {
    return (
        <SafeAreaView style={{ flex: 1 }}>
          <View style={styles.container}>
            <View style={{ marginBottom: 18 }}>
              <Text style={styles.title}>Sign up</Text>
              <Text style={styles.subtitle}>Please sign up to get full access</Text>
            </View>
            <View style={{ flex: 2 }}>
              {this.renderInputs()}
              <View style={{ alignItems: 'center' }}>
                <View style={styles.divider}>
                  <Text style={styles.dividerLabel}>or</Text>
                </View>
              </View>
              {this.renderSocials()}
            </View>
            <View style={{ flex: 0.25, alignItems: 'center' }}>
              <Text
                  style={{
                    fontSize: SIZES.FONT,
                    color: COLORS.GREY,
                    marginBottom: SIZES.BASE,
                  }}>
                Already have an account?
              </Text>
              <TouchableOpacity onPress={() => this.openSignin()}>
                <Text
                    style={{
                      fontSize: SIZES.FONT,
                      fontWeight: '600',
                      color: COLORS.BLUE,
                    }}>
                  Signin!
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: SIZES.BASE,
    justifyContent: 'center',
    padding: SIZES.PADDING / 0.83,
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.PADDING,
    paddingVertical: SIZES.PADDING * 2,
  },
  divider: {
    alignItems: 'center',
    backgroundColor: COLORS.GREY,
    height: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    marginVertical: SIZES.PADDING * 2,
    width: '50%',
  },
  dividerLabel: {
    backgroundColor: COLORS.WHITE,
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
    paddingHorizontal: SIZES.BASE,
    position: 'absolute',
  },
  facebook: {
    backgroundColor: COLORS.FACEBOOK,
    flex: 1,
    paddingVertical: SIZES.PADDING * 1.33,
  },
  input: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
  },
  inputContainer: {
    marginBottom: SIZES.PADDING,
  },
  label: {
    color: COLORS.DARK_GREY,
    fontSize: SIZES.FONT,
    marginBottom: SIZES.BASE,
  },
  signin: {
    paddingVertical: SIZES.PADDING * 1.33,
  },
  social: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  socialLabel: {
    color: COLORS.WHITE,
    flex: 1,
    fontWeight: '500',
    textAlign: 'center',
  },
  subtitle: {
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
  },
  title: {
    fontSize: SIZES.TITLE,
    fontWeight: '600',
    letterSpacing: 1,
    marginBottom: SIZES.BASE,
  },
});







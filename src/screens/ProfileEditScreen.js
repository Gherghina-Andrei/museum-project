import React ,{ Component, useContext } from 'react';
import { database } from '../../config/firebase';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity, Alert, TextInput
} from 'react-native';
import { UserContext } from '../../config/context';
import BackButton from "../components/BackButton";



class Profile extends Component{

    _isMounted = false;

    static navigationOptions = ({ navigation }) => {

        return {
            title: '',
            headerTransparent: 'true',
            headerLeft: () =>
                <BackButton
                    onPress={() => {
                        navigation.goBack();
                    }}
                />


        };
    };
    constructor(props) {
        super(props);
        this.state = {
            User: '',
            firstName: '',
            lastName: '',
        };
    }

    static contextType = UserContext

    componentDidMount() {
        this._isMounted = true;
        const userContext = this.context

        this.setState({ User: userContext.state.User })



    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    updateProfile() {

        const { navigation } = this.props;
        const updateRef = database.ref('/users/' + this.state.User.uid).update({
            firstName: this.state.firstName,
            lastName: this.state.lastName,
        })

        const userContext = this.context
        userContext.setName(this.state.firstName,this.state.lastName);

        this.setState({User: {
            firstName: this.state.firstName,
            lastName: this.state.lastName
            } })





        this.props.navigation.navigate('ProfileScreen');

    }

    openLogin = () => {
        this.props.navigation.navigate('Login');
    };


    updateTextInput = (text, field) => {
        const state = this.state
        state[field] = text;
        this.setState(state);
    }

    createTwoButtonAlert = () =>
        Alert.alert(
            "Warning",
            "Are you sure you want to save changes?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => this.updateProfile() }
            ],
            { cancelable: false }
        );



    render(){

        return (
            <UserContext.Consumer>
                {
                    profile =>
                        <View style={styles.container}>

                            <View style={styles.header}></View>
                            <Image style={styles.avatar} source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
                            <View style={styles.body}>
                                <View style={styles.bodyContent}>
                                    <TextInput style={styles.firstName}
                                        placeholder={this.state.User.firstName}
                                        value={this.state.firstName}
                                        onChangeText={(text) => this.updateTextInput(text, 'firstName')}
                                    />

                                    <TextInput style={styles.lastName}
                                               placeholder={this.state.User.lastName}
                                               value={this.state.lastName}
                                               onChangeText={(text) => this.updateTextInput(text, 'lastName')}
                                    />


                                    <TouchableOpacity style={styles.saveContainer} onPress={() => this.createTwoButtonAlert()}>
                                        <Text>Save</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                }
            </UserContext.Consumer>
        );
    }
}
const styles = StyleSheet.create({
    header:{
        backgroundColor: "#1d2226",
        height:150,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom:10,
        alignSelf:'center',
        position: 'absolute',
        marginTop:100
    },
    name:{
        fontSize:22,
        color:"#001c26",
        fontWeight:'600',
    },
    body:{
        marginTop:40,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding:30,
    },
    info:{
        fontSize:16,
        color: "#00BFFF",
        marginTop:10
    },
    firstName: {
        marginTop:70,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
        backgroundColor: "#89bdff",
        textAlign:"center"

    },
    lastName: {
        marginTop:20,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
        backgroundColor: "#89bdff",
        textAlign:"center"

    },
    saveContainer: {
        marginTop:20,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
        backgroundColor: "#89bdff",

    },
});

export default Profile;
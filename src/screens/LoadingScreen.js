import React from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

const LoadingScreen = ({navigation}) => {

  return(
    <View>

      <Text style={styles.text}>
        Hello there!
      </Text>

      <Button
      onPress = {() => navigation.navigate('Login')}
      title='Go to login'>
      </Button>

    </View>

  );


};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default LoadingScreen;

import React, { Component } from 'react';
import { View, Text, FlatList, StatusBar } from 'react-native';
import { Card, SearchBar } from 'react-native-elements';
import {f, auth, database} from '../../config/firebase';
import { CardTwelve } from 'react-native-card-ui';
import {ExhibitCard} from "../components/ExhibitCard";
import MenuImage from "../components/MenuImage";


export default class ExhibitionListScreen extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: 'Home',

        headerLeft: () =>
            <MenuImage
                onPress={() => {
                    navigation.openDrawer();
                }}
            />

    });


  constructor (props) {

    super(props);

    StatusBar.setHidden(true);
    this.state = {
        page: 1,
        isLoading: true,
        dataSource: [],
        refreshing: false,
        search: '',
        minLength: 3,
     
    };
  }


  componentDidMount () {
    this.makeRequest();
  }

  makeRequest  = () => {

    const { search, minLength } = this.state;

    /*
     if (search && search.length < minLength)
     {
        return null;
     }
    */

      const ref = database.ref('exhibition');

      ref.once('value').then((snapshot) => {
          this.setState({ dataSource: Object.values(snapshot.val()) });
      });

   


  }


  emptyList = () => {
    return(
      <View>
        <Text style={{fontSize: 30}}>Nothing found!</Text>
      </View>
    )

  }


    _onPressItem = (exhibit) => {
        this.props.navigation.navigate('ExhibitionDetailScreen',{
            exhibit: exhibit   //exhibit info
        })

    };

  _onPressBook = (exhibit) => {
      this.props.navigation.navigate('CalendarScreen',{
          exhibit: exhibit   //exhibit info
      })
  };

  renderItem = ({item, index}) => {

    if (!item) {
      return null
    }

    return (
      <View >
          <ExhibitCard
              title={item.title}
              description={item.description}
              price={null}
              image={{uri: item.image}}
              buttonText={"VIEW DETAILS"}
              buttonColor={"#4383FF"}
              onClickButton={()=>{this._onPressItem(item)}}
              onClickBook = {() => {this._onPressBook(item)}}
          />

      </View>
    );
  }

  keyExtractor = (item, index) => {
    return index.toString();
  }

  updateSearch = search => {


    this.setState({ search, dataSource: [], page: 1 });


      this.makeRequest(search);


  };

  handleLoadMore = () => {

    this.setState({ page: this.state.page + 1}, this.makeRequest)

  }

  handleRefresh = () => {

    this.setState({
      page: 1,
      refreshing: true,
      search: '',
      dataSource: [],

    }, this.makeRequest(this.state.search))

  }
 

  render () {
    const { search } = this.state;
    return (
     
      <View style={{flex: 1}}>


        <FlatList
          data={this.state.dataSource}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.5}
          onRefresh={this.handleRefresh}
          
        />
      </View>
    );
  }
}


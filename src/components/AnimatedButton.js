import React, { Component } from 'react';
import {
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import * as Animatable from 'react-native-animatable';
export default class AnimatedButton extends Component {
    constructor(props) {
        super(props);
        this.state ={
            status: false
        }
    }
    _onPress(){
        this.props._onPress(!this.state.status)
        this.setState({ status: !this.state.status})
        switch (this.props.effect) {
            case 'bounce':
                this.view.bounce(800)
                break;
            case 'flash':
                this.view.flash(800)
                break;
            case 'jello':
                this.view.jello(800)
                break;
            case 'pulse':
                this.view.pulse(800)
                break;
            case 'rotate':
                this.view.rotate(800)
                break;
            case 'rubberBand':
                this.view.rubberBand(800)
                break;
            case 'shake':
                this.view.shake(800)
                break;
            case 'swing':
                this.view.swing(800)
                break;
            case 'tada':
                this.view.tada(800)
                break;
            case 'wobble':
                this.view.wobble(800)
                break;
        }

    }
    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this._onPress()}>
                <Animatable.View ref="view" style={{ margin:10, paddingTop :10, paddingBottom: 10, paddingRight: 20, paddingLeft: 20, backgroundColor: this.state.status ? this.props.onColor : "#bdbdbd", borderRadius:20}}>
                    <Text style={{color: this.state.status ? "white" : "#696969", fontWeight: "bold"}}>{this.props.text}</Text>
                </Animatable.View>
            </TouchableWithoutFeedback>
        );
    }
}